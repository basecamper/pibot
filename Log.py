import time
import codecs



logfile = "./phone.log"

def log(message:str):
	print(message)
	return True

def logWrite(message:str):
	def writeLog(m):
		with codecs.open(logfile, "a", encoding="utf-8") as file:
			file.write(m)
	lm = u"%s   %s" % (time.ctime(), message)
	writeLog(lm+'\n')

def logRead() -> str:
	f = open(logfile,'r')
	data = f.read()
	f.close()
	return data