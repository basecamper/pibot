from codecs import decode

from Phone.ResultParser import ResultParser as RP

from Log import log

from Message.Message import Message

from Message import TYPES, TARGETS


class PhoneParser:
	


	@staticmethod
	def _splitValues(vdata):
		return vdata.replace('"','').split(',')
	
	@staticmethod
	def _parseData(kv:str, message:Message):
		#log("PhoneParser._parse(): kv: %s " % kv)
		k = ""
		v = ""
		s = kv.split(": ")
		
		if len(s) > 0:
			k = s[0]
			if len(s) > 1:
				v = s[1]
				
		if 'RING' in k:
			message.writeToLog = True
			message.type = TYPES.PHONE_DATA_RING
			
		elif 'NO CARRIER' in k:
			message.type = TYPES.PHONE_DATA_NO_CARRIER
			
		elif 'OK' in k:
			message.type = TYPES.PHONE_ANSWER_OK
			
		elif 'ERROR' in k:
			message.type = TYPES.PHONE_ANSWER_ERROR
			message.data["status"] = "ERROR"
		
		elif v and len(v):
			vs = PhoneParser._splitValues(v)
			
			if k == "+CPAS":
				message.data["activity"] = RP.parse("pas", vs[0])
			
			elif k == "+CCLK":
				message.data["clock"] = RP.parse("time",v)
				
			elif k == "+CSQ":
				message.data["signal"] = {}
				message.data["signal"]["rssi"] = RP.parse("rssi",vs[0])
				message.data["signal"]["rxqual"] = RP.parse("ber",vs[1])
			
			elif k == "+CBC":
				message.data["battery"] = {}
				message.data["battery"]["status"] = RP.parse("bcs",vs[0])
				message.data["battery"]["remaining"] = RP.parse("bcl",vs[1])
				message.data["battery"]["volt"] = RP.parse("voltage",vs[2])
			
			elif k == "+CSCS":
				message.data["chset"] = RP.parse("string",v)
				
			elif k =='+CLIP':
				message.writeToLog = True
				message.data["clip"] = {}
				message.data["clip"]["number"] = RP.parse('number',vs[0])
				message.data["clip"]["type"] = RP.parse('number',vs[1])
				message.data["clip"]["subaddr"] = RP.parse('number',vs[2])
				message.data["clip"]["satype"] = RP.parse('number',vs[3])
				message.data["clip"]["entry"] = RP.parse('string',vs[4])
				message.data["clip"]["validity"] = RP.parse('number',vs[5])
				
			elif k == '+CMTI':
				message.writeToLog = True
				message.data["sms"] = {}
				message.data["sms"]["entry"] = RP.parse('string',vs[0])
				message.data["sms"]["num"] = RP.parse('number',vs[1])
				
			else:
				if not "data" in message.data:
					message.data["data"] = {}
				message.data["data"][k] = v
		else:
			if k:
				message.data["text"] = k
		
			
		
	
	@staticmethod
	def sanitizeData(data:bytes) -> str:
		return decode(data, "utf-8")
	
	@staticmethod
	def parse(data:bytes) -> Message:
		message = Message(type=TYPES.PHONE_DATA_UNKNOWN,
						  source=TARGETS.PHONE,
						  destination=TARGETS.BOT,
						  data={})
		dataArray = data.split(b'\r\n')
		
		for part in dataArray:
			if len(part):
				PhoneParser._parseData(PhoneParser.sanitizeData(part), message)
		
		#log("PhoneParser.parse(): returning: %s" % message)
		return message