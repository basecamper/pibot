from Message.Message import Message
from Message import TYPES
from Message import TARGETS
from Phone.ResultParser import ResultParser as RP

from Log import log

from codecs import decode


class AnswerParser:
	@staticmethod
	def _parseSplittedAnswer(array:list):
		data = {}
		
		c = 0
		while c < len(array):
			
			k ,v = array[c]
			
			#log("AnswerParser(): k:\"%s\" v:\"%s\"" % (k,v))
			
			v = v.replace('"','')
			
			vs = v.split(',') if v and len(v) else []
			
			if k == "+CPAS":
				data["activity"] = RP.parse("pas", v)
			
			elif k == "+CSQ":
				data["signal"] = {}
				data["signal"]["rssi"] = RP.parse("rssi",vs[0])
				data["signal"]["rxqual"] = RP.parse("ber",vs[1])
			
			elif k == "+CBC":
				data["battery"] = {}
				data["battery"]["status"] = RP.parse("bcs",vs[0])
				data["battery"]["remaining"] = RP.parse("bcl",vs[1])
				data["battery"]["volt"] = RP.parse("voltage",vs[2])
			
			elif k == "+CCLK":
				data["+CCLK"] = RP.parse("time",v)
				
			elif k == "+CSCS":
				data["ok"] = {}
				data["ok"]["chset"] = RP.parse("string",v)
			
			elif k == "+CMGL":
				if 'smslist' not in data:
					data["smslist"] = {}
				
				meta_id = vs[0]
				
				data["smslist"][meta_id] = {}
				data["smslist"][meta_id]["status"] = vs[1]
				data["smslist"][meta_id]["sender"] = vs[2]
				data["smslist"][meta_id]["entry"] = vs[3]
				data["smslist"][meta_id]["timestamp"] = vs[4]+','+vs[5]
				
				c += 1
				
				if c < len(array):
					
					text, v = array[c]
					
					temp = ""
					#log("smstext: %s" % text)
					try:
						temp = decode(text, "hex")
						temp = decode(temp, "latin_1")
						
					except Exception as e:
						#log("AnswerParser(): sms text exception %s" % e)
						temp = text
							
					data["smslist"][meta_id]["text"] = temp
					
					log("AnswerParser._parseAnswerPart(): SMS: %s" % data["smslist"][meta_id]["text"])
			
			else:
				data["other"] = {}
				data["other"][k] = v
			
			c += 1
		
		return data
	
	@staticmethod
	def _prepareArray(rawData:list) -> list:
		parserArray = []
		if len(rawData) > 1:
			for part in rawData[:-1]:
				
				splitted = part.split(": ")
				
				key = splitted[0]
				value = ""
				if (len(splitted) > 1):
					value = splitted[1]
				
				parserArray.append((key, value))
		
		return parserArray
	
	@staticmethod
	def parse(data:str) -> Message:
		#log("AnswerParser.parse(): array: %d len" % len(array))
		
		array = data.split('\r\n')
		
		answerType = TYPES.PHONE_ANSWER_UNKNOWN
		data = {}
		
		if len(array) == 0:
			answerType = TYPES.PHONE_ANSWER_TIMEOUT
			
		elif len(array) > 0:
			
			if "OK" == array[-1]:
				answerType = TYPES.PHONE_ANSWER_OK
			elif "ERROR" == array[-1]:
				answerType = TYPES.PHONE_ANSWER_ERROR
			elif "" == array[-1] and len(array) == 1:
				answerType = TYPES.PHONE_ANSWER_EMPTY
		
			if len(array) > 1:
				data = AnswerParser._parseSplittedAnswer(
					AnswerParser._prepareArray(array)
				)
				
		return Message(type=answerType, data=data)