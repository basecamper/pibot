import asyncio

from BotQueue import BotQueue
from Message.Factory import Factory


class PhoneLLA(asyncio.Protocol):
	
	connection = None
	buffer = bytes(1024)
	
	def connection_made(self, transport):
		PhoneLLA.connection = self
		self.transport = transport
		print('PhoneLLA port opened', transport)
	
	def data_received(self, data):
		#print('PhoneLLA data received', data)
		PhoneLLA.buffer += data
		self.reviewBuffer()
		
	def reviewBuffer(self):
		if PhoneLLA.buffer[-2:] == b'\r\n':
			BotQueue.put(Factory.fromPhone(PhoneLLA.buffer))
			PhoneLLA.buffer = b''
	
	def connection_lost(self, exc):
		print('PhoneLLA port closed')
		PhoneLLA.connection = None
		self.transport.close()