class SmsControls {
    constructor() {
        var bSmsReadAll = create_element("div",{className:"button", innerHTML:"unread", onclick:function(e) {
            if (bSmsReadAll.innerHTML == "all") {
                bSmsReadAll.innerHTML = "unread";
            } else {
                bSmsReadAll.innerHTML = "all";
            }
        }});
        var bSmsRead = create_element("div",{className:"button", innerHTML:"read", onclick:function(e) {
            var msg = "";
            if (bSmsReadAll.innerHTML == "all") {
        
                msg = "phone,+CMGL=\"ALL\""
            } else {
                msg = "phone,+CMGL=\"REC UNREAD\""
            }
            print("Requesting " + bSmsReadAll.innerHTML + " sms:");
            socket.send(msg);
        }});
        var iSmsDeleteNum = create_element("input",{id:"deleteSmsNum", type:"text", placeholder:"id", size:3});
        var bSmsDeleteAll = create_element("div",{className:"button", innerHTML:"all", onclick:function(e) {
            print("Deleting all sms");
            socket.send("phone,+CMGD=21,1");
        }});
        var bSmsDeleteNum = create_element("div",{className:"button", innerHTML:"delete:", onclick:function(e) {
            print("Deleting sms:");
            var num = iSmsDeleteNum.value;
            socket.send("phone,+CMGD="+num+",0");
        }});

        this.iSmsSendEntry = create_element("input",{id:"smsSendNum", type:"text", placeholder:"number", value:"", size:20});
        var iSmsSendEntry = this.iSmsSendEntry;
        
        var iSmsSendText = create_element("input",{id:"smsSendText", type:"text", placeholder:"txt", size:40});
        var bSmsSend = create_element("div",{className:"button", innerHTML:"send", onclick:function(e) {
            print("Sending sms");
            socket.send("phone,+CMGS=\""+iSmsSendEntry.value+"\"\n"+iSmsSendText.value +"\n"+ String.fromCharCode(26));
        }});

        var iChset = create_element("input",{id:"smsSendChset", type:"text", placeholder:"charset", value:"8859-1", size:8});
        var bSendChset = create_element("div",{className:"button", innerHTML:"set", onclick:function(e) {
            print("Sending chset:",iChset.value );
            socket.send("phone,+CSCS=\""+iChset.value+"\"");
        }});
        
        this.container = create_element("div", {id:"smsContainer", className:"shown", innerHTML:"Sms: "}); 
        
        this.container.appendChild( iChset );
        this.container.appendChild( bSendChset );
        this.container.appendChild( create_placeholder() );
        this.container.appendChild( bSmsRead );
        this.container.appendChild( bSmsReadAll );
        this.container.appendChild( create_placeholder() );
        this.container.appendChild( this.iSmsSendEntry );
        this.container.appendChild( iSmsSendText );
        this.container.appendChild( bSmsSend );
        this.container.appendChild( create_placeholder() );
        this.container.appendChild( bSmsDeleteNum );
        this.container.appendChild( iSmsDeleteNum );
        this.container.appendChild( create_placeholder("or") );
        this.container.appendChild( bSmsDeleteAll );
    }
}

